web site url https://organic-shop-32039.firebaseapp.com
firebase project console url https://console.firebase.google.com/project/organic-shop-32039/overview
bitbucket repository url https://benjaminrachi@bitbucket.org/benjaminrachi/organic-shop.git
course project repository url https://github.com/mosh-hamedani/organic-shop

// stripe
Publishable
pk_test_ukwly2LS9hq7t2BM4cgAUnxi00fquyq65n
Secret
sk_test_sJaV03rLL5xx8p4wzVnOMXym00h0Whuh8J
To test things out, use card number 4242 4242 4242 4242 with any expiration date in the future, any 3-digit number for the CVC and any valid zip code.
# OrganicShop

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.17.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
