import { ModalContent } from '../../../shared/models/modal-content';
import { ToastService } from '../../../shared/services/toast.service';
import { ProductService } from '../../../shared/services/product.service';
import { Component, OnInit } from '@angular/core';
import { Product } from 'shared/models/product';
import { Observable } from 'rxjs';
import { ModalService } from 'shared/services/modal.service';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.sass']
})
export class AdminProductsComponent implements OnInit {

  products$: Observable<Product[]>;

  constructor(
    private productService: ProductService,
    private toastService: ToastService,
    private modalService: ModalService) { }

  ngOnInit() {
    this.products$ = this.productService.getAllProducts();
  }

  delete(productId) {
    const deleteCallBack = function() {
      return this.productService.delete(productId)
        .then(res => this.toastService.showSuccess('Product successfully deleted'))
        .catch(err => this.toastService.showError());
    };

    const modalContent: ModalContent = {
      title: 'Delete a product',
      body: `
        Are you sure you want to delete this product? /n
        All information associated to this product will be permanently deleted./n
        This operation can not be undone.
      `,
      actionButtons: [
        {
          name: 'Delete',
          classes: 'btn btn-danger float-right',
          click: deleteCallBack.bind(this)
        }
      ]

    };

    this.modalService.open(modalContent);

  }

}
