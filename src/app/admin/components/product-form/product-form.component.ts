import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductService } from '../../../shared/services/product.service';
import { ToastService } from '../../../shared/services/toast.service';
import { CategoryService } from '../../../shared/services/category.service';
import { Product } from '../../../shared/models/product';
import { Category } from '../../../shared/models/category';

@Component({
  selector: 'product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.sass']
})
export class ProductFormComponent implements OnInit {
  @Input() product: Product;
  @Input() id: string;
  categories$: Observable<Category[]>;

  constructor(
    private categoryService: CategoryService,
    private productService: ProductService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.categories$ = this.categoryService.getAllCategories();
  }

  save() {
    if (!this.id) {
      this.create();
      return;
    }
    this.update();
  }

  create() {
    this.productService.create(this.product)
      .then(res => this.toastService.showSuccess('Product successfully created'))
      .catch(err => this.toastService.showError());
  }

  update() {
    this.productService.update(this.id, this.product)
      .then(res => this.toastService.showSuccess('Product successfully updated'))
      .catch(err => this.toastService.showError());
  }

  delete() {
    this.productService.delete(this.id)
      .then(res => this.toastService.showSuccess('Product successfully deleted'))
      .catch(err => this.toastService.showError());
  }
}
