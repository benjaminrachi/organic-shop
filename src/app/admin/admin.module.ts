import { NgModule } from '@angular/core';
import { SharedModule } from 'shared/shared.module';
import { CustomFormsModule } from 'ng2-validation';
import { AdminRoutingModule } from './admin-routing.module';

// guards
import { AdminGuard } from './guards/admin-guard.guard';

// pages
import { AdminProductsComponent } from './pages/admin-products/admin-products.component';
import { AdminOrdersComponent } from './pages/admin-orders/admin-orders.component';
import { ProductDetailsViewComponent } from './pages/product-details-view/product-details-view.component';

// components
import { ProductFormComponent } from './components/product-form/product-form.component';


@NgModule({
  declarations: [
    AdminProductsComponent,
    AdminOrdersComponent,
    ProductDetailsViewComponent,
    ProductFormComponent,
  ],
  imports: [
    SharedModule,
    CustomFormsModule,
    AdminRoutingModule
  ],
  exports: [],
  providers: [
    AdminGuard
  ]
})
export class AdminModule { }
