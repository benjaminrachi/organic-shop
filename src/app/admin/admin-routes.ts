import { ProductDetailsViewComponent } from './pages/product-details-view/product-details-view.component';
import { AdminGuard } from './guards/admin-guard.guard';
import { AuthGuard } from 'shared/guards/auth-guard.guard';
import { AdminOrdersComponent } from './pages/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './pages/admin-products/admin-products.component';

export const routes = [
  {
    path: 'admin/products/new',
    component: ProductDetailsViewComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/products/:id',
    component: ProductDetailsViewComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/products',
    component: AdminProductsComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/orders',
    component: AdminOrdersComponent,
    canActivate: [AuthGuard, AdminGuard]
  }

];
