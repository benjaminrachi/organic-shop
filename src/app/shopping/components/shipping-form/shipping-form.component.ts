import { ShippingDetails } from '../../../shared/models/order';
import { ShoppingCart } from '../../../shared/models/shopping-cart';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.sass']
})
export class ShippingFormComponent {
  @Input() userId: string;
  @Input() cart: ShoppingCart;
  @Output() proceedToCheckoutCalled = new EventEmitter<{
    userId: string,
    shippingDetails: ShippingDetails,
    cart: ShoppingCart
  }>();
  shippingDetails = {} as ShippingDetails;

  constructor() { }

  proceedToCheckout() {
    this.proceedToCheckoutCalled.emit({
      userId: this.userId,
      shippingDetails: this.shippingDetails,
      cart: this.cart
    });
  }
}
