import { ShoppingCartService } from '../../../shared/services/shopping-cart.service';
import { ShoppingCart } from '../../../shared/models/shopping-cart';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'shopping-cart-summary',
  templateUrl: './shopping-cart-summary.component.html',
  styleUrls: ['./shopping-cart-summary.component.sass']
})
export class ShoppingCartSummaryComponent implements OnInit {
  @Input() cart: ShoppingCart;

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
  }

  getTotalPdtQtyInCart(): number {
    return this.shoppingCartService.getTotalPdtQtyInCart();
  }

  getCartTotalPrice(): number {
    return this.shoppingCartService.getCartTotalPrice();
  }
}
