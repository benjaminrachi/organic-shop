import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Product } from 'shared/models/product';
import { ProductService } from '../../../shared/services/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  products: Product[] = [];
  filteredProducts: Product[] = [];
  selectedCategoryId: string;

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.populateProducts()
      .subscribe(routeQueryParams => {
        this.selectedCategoryId = routeQueryParams.category;
        this.filterProductsByCategory();
      });
  }

  populateProducts() {
    return this.productService.getAllProducts()
      .pipe(
        switchMap(products => {
          this.filteredProducts = this.products = products;
          return this.route.queryParams;
        })
      );
  }

  filterProductsByCategory() {
    this.filteredProducts = this.selectedCategoryId
      ? this.products.filter(product => product.categoryId === this.selectedCategoryId)
      : this.products;
  }
}
