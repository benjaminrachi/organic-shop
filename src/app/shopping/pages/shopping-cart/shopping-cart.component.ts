import { ToastService } from '../../../shared/services/toast.service';
import { ShoppingCart } from '../../../shared/models/shopping-cart';
import { Observable } from 'rxjs';
import { ShoppingCartService } from '../../../shared/services/shopping-cart.service';
import { Component, OnInit } from '@angular/core';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.sass']
})
export class ShoppingCartComponent implements OnInit {
  faTrash = faTrash;
  cart$: Observable<ShoppingCart>;
  constructor(private shoppingCartService: ShoppingCartService, private toastService: ToastService) { }

  ngOnInit() {
    this.cart$ = this.shoppingCartService.shoppingCartBehaviorSubject.asObservable();
  }

  getTotalPdtQtyInCart(): number {
    return this.shoppingCartService.getTotalPdtQtyInCart();
  }

  getCartTotalPrice(): number {
    return this.shoppingCartService.getCartTotalPrice();
  }

  async clearCart() {
    await this.shoppingCartService.clearCart();
    this.toastService.showSuccess('Cart successfully deleted');
  }
}
