import { Payment, CreditCardDetails } from '../../../shared/models/payment';
import { PaymentStripeService } from '../../../shared/services/payment-stripe.service';
import { User } from '../../../shared/models/user';
import { Router } from '@angular/router';
import { Order, ShippingDetails } from '../../../shared/models/order';
import { AuthService } from '../../../shared/services/auth.service';
import { OrderService } from '../../../shared/services/order.service';
import { ShoppingCart } from '../../../shared/models/shopping-cart';
import { ShoppingCartService } from '../../../shared/services/shopping-cart.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PaymentService } from 'shared/services/payment.service';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.sass']
})
export class CheckOutComponent implements OnInit {
  cart$: Observable<ShoppingCart>;
  user$: Observable<User>;
  orderId: string = null;
  order: Order;

  constructor(
    private orderService: OrderService,
    private paymentService: PaymentService,
    private shoppingCartService: ShoppingCartService,
    private authService: AuthService,
    private paymentStripeService: PaymentStripeService,
    private router: Router) { }

  ngOnInit() {
    this.cart$ = this.shoppingCartService.shoppingCartBehaviorSubject.asObservable();
    this.user$ = this.authService.user$;
    this.paymentStripeService.initHandler(this.savePaymentAndUpdateOrder.bind(this));
  }

  async placeOrder(userId: string, shippingDetails: ShippingDetails, cart: ShoppingCart): Promise<Order> {
    this.order = new Order(userId, shippingDetails, cart);
    this.orderId = await this.orderService.createOrder(this.order);
    return this.order;
  }

  async proceedToCheckout(data) {
    const { userId, shippingDetails, cart } = data;
    const order = await this.placeOrder(userId, shippingDetails, cart);
    this.paymentStripeService.checkout(order, this.orderId);
  }

  async savePaymentAndUpdateOrder(data: {
    id: string,
    card: { exp_month: string, exp_year: string, last4: string, country: string, brand: string}
  }) {
    console.log('payment by strip call back', data);
    // save payment in the payment table with orderId
    const creditCardDetails: CreditCardDetails = {
      exp_month: data.card.exp_month,
      exp_year: data.card.exp_year,
      last4: data.card.last4,
      country: data.card.country,
      brand: data.card.brand
    };

    const payment: Payment = {
      apiId: data.id,
      apiName: 'STRIPE',
      userId: this.order.userId,
      createdDate: new Date(),
      orderId: this.orderId,
      amount: this.order.totalPrice,
      status: 'ACCEPTED',
      creditCardDetails,
    };

    const paymentId = await this.paymentService.createPayment(payment);

    // update the order status and save attach the payment token
    const newOrder = { ...this.order, status: 'PAID' };
    this.orderService.updateOrder(newOrder, this.orderId);

    // redirect to order success
    this.router.navigate(['order-success', this.orderId]);
  }
}
