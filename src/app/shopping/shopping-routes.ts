import { AuthGuard } from 'shared/guards/auth-guard.guard';
import { MyOrdersComponent } from './pages/my-orders/my-orders.component';
import { LoginComponent } from '../core/pages/login/login.component';
import { OrderSuccessComponent } from './pages/order-success/order-success.component';
import { CheckOutComponent } from './pages/check-out/check-out.component';
import { ShoppingCartComponent } from './pages/shopping-cart/shopping-cart.component';
import { HomeComponent } from './pages/home/home.component';

export const routes = [
  // ANONYMOUS USER
  ///////////////////////////////
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'shopping-cart',
    component: ShoppingCartComponent
  },

  // LOGGED IN USER
  ///////////////////////////////
  {
    path: 'check-out',
    component: CheckOutComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'my/orders',
    component: MyOrdersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'order-success/:orderId',
    component: OrderSuccessComponent,
    canActivate: [AuthGuard]
  },

];
