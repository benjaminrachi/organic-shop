import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AdminModule } from './admin/admin.module';
import { SharedModule } from './shared/shared.module';
import { ShoppingModule } from './shopping/shopping.module';
import { CoreModule } from './core/core.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';

// pages
import { AppComponent } from './app.component';

// components
import { ModalComponent } from './core/components/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),

    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,

    SharedModule,
    AdminModule,
    ShoppingModule,
    CoreModule

  ],
  entryComponents: [ModalComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// https://www.techiediaries.com/angular-router/
// https://www.techiediaries.com/angular-lazy-load-module-example/

// https://grokonez.com/frontend/angular/angular-8-firestore-tutorial-crud-examples-angularfire
// https://www.techiediaries.com/angular-firestore-tutorial/
// https://itnext.io/how-to-crud-in-angular-firebase-firestore-456353d7c62
