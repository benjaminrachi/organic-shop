import { ShoppingCartService } from '../../../shared/services/shopping-cart.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.sass']
})
export class NavBarComponent implements OnInit, OnDestroy {
  faShoppingCart = faShoppingCart;
  collapsed = true;

  constructor(public authService: AuthService, private router: Router, private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
  }

  getTotalPdtQtyInCart(): number {
    return this.shoppingCartService.getTotalPdtQtyInCart();
  }

  logout() {
    this.authService.logout()
      .then(() => this.router.navigateByUrl('/'));
  }

  ngOnDestroy() {
  }
}
