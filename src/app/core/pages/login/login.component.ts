import { AuthService } from '../../../shared/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParamMap
      .subscribe(queryParams => {
        this.returnUrl = queryParams.get('returnUrl');
      });
  }

  loginWithGoogle() {
    this.authService.loginWithGoogle()
      .then(res => {
        console.log('login succes', res);
        this.router.navigateByUrl(this.returnUrl || '/');
      })
      .catch(err => console.error('login failed', err));
  }
}
