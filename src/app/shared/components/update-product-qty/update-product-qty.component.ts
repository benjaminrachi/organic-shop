import { Product } from 'shared/models/product';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'update-product-qty',
  templateUrl: './update-product-qty.component.html',
  styleUrls: ['./update-product-qty.component.sass']
})
export class UpdateProductQtyComponent {
  @Input() product: Product;

  constructor(private shoppingCartService: ShoppingCartService) { }

  updateProductQtyInCart(qtyToAdd: number) {
    this.shoppingCartService.updateProductQtyInCart(this.product, qtyToAdd);
  }

  getProductQtyInCart(): number {
    return this.shoppingCartService.getProductQtyInCart(this.product.id);
  }

}
