import { ShoppingCartService } from '../../services/shopping-cart.service';
import { Product } from '../../models/product';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.sass']
})
export class ProductCardComponent {
  @Input() product: Product;
  @Input() showActions: boolean;

  constructor(private shoppingCartService: ShoppingCartService) { }

  showProductCard() {
    return Object.keys(this.product).length > 0;
  }

  updateProductQtyInCart(qtyToAdd: number) {
    this.shoppingCartService.updateProductQtyInCart(this.product, qtyToAdd);
  }

  getProductQtyInCart(): number {
    return this.shoppingCartService.getProductQtyInCart(this.product.id);
  }
}
