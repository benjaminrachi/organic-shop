import { OrderService } from '../../services/order.service';
import { Order } from '../../models/order';
import { Observable } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'orders-table',
  templateUrl: './orders-table.component.html',
  styleUrls: ['./orders-table.component.sass']
})
export class OrdersTableComponent implements OnInit {
  orders$: Observable<Order[]>;
  @Input() displayUserOrders: boolean;

  constructor(private orderService: OrderService) {

    if (this.displayUserOrders) {
      this.orders$ = this.orderService.getOrdersByUserId();
    } else {
      this.orders$ = this.orderService.getAllOrders();
    }
   }

  ngOnInit() {
  }

}
