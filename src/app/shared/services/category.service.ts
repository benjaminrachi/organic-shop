import { Category } from '../models/category';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private afs: AngularFirestore) { }

  getAllCategories() {
    return this.afs.collection<Category>('categories').valueChanges({idField: 'id'});

  }
}
