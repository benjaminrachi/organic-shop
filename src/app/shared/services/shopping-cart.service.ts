import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ToastService } from './toast.service';
import { ShoppingCart, ShoppingCartItem } from '../models/shopping-cart';
import { AuthService } from './auth.service';
import { Product } from '../models/product';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  userId: string;
  shoppingCartsCollection: AngularFirestoreCollection<ShoppingCart>;
  shoppingCartId: string;
  shoppingCartDocument: AngularFirestoreDocument<ShoppingCart>;
  shoppingCartBehaviorSubject = new BehaviorSubject<ShoppingCart>(null);

  constructor(
    private afs: AngularFirestore,
    private auth: AuthService,
    private toastService: ToastService) {

    this.shoppingCartsCollection = this.afs.collection<ShoppingCart>('shopping-carts');
    this.auth.user$.subscribe(user => {
      this.userId = user && user.uid;
    });
    this.init();

  }

  async init() {
    this.shoppingCartId = await this.getUserCartIdFromDBAndStorage();
    // getting the cartdoc only if cartId exist -- dont create a new one
    if (this.shoppingCartId) {
      await this.getOrCreateCartDoc();
    }
  }

  async updateProductQtyInCart(product: Product, qtyToAdd: number) {
    if (!this.shoppingCartDocument) {
      await this.getOrCreateCartDoc();
    }
    const cart = this.shoppingCartBehaviorSubject.value || this.getDefaultCartObject();
    cart.userId = (this.userId || null); // if the cart was created in anonymous mode
    cart.lastUpdateDate = new Date();

    let item = this.getItemInCartByProductId(product.id);
    if (!item) {
      item = this.addNewItemToCart(cart, product);
    }
    item.quantity = (item ? item.quantity : 0) + qtyToAdd;
    await this.shoppingCartDocument.set(cart, { merge: true });
    this.toastService.showSuccess(`Product successfully ${qtyToAdd > 0 ? 'added to' : 'removed from'} cart`);
  }

  addNewItemToCart(cart: ShoppingCart, product: Product) {
    const item = {
      product,
      quantity: 0
    } as ShoppingCartItem;

    cart.items.push(item);
    return item;
  }

  async getUserCartIdFromDBAndStorage(): Promise<string> {
    // looking in the db
    {
      const cartCollectionForUser = await this.afs.collection<ShoppingCart>('shopping-carts',
        cart => cart.where('userId', '==', (this.userId || '')).where('userId', '>', '')).get().toPromise();

      if (cartCollectionForUser && cartCollectionForUser.docs && cartCollectionForUser.docs[0] && cartCollectionForUser.docs[0].id) {
        const cartIdFromDb = cartCollectionForUser.docs[0].id;
        return cartIdFromDb;
      }
    }
    // looking in the storage
    {
      const cartIdFromStorage = localStorage.getItem('shopping-cart-id');

      if (cartIdFromStorage) {
        return cartIdFromStorage;
      }
    }
    return null;
  }

  getItemInCartByProductId(productId: string): ShoppingCartItem {
    const cart = this.shoppingCartBehaviorSubject.value;
    if (!cart) {
      return null;
    }
    const item = cart.items.find(i => i.product.id === productId);
    return item;
  }

  getProductQtyInCart(productId: string): number {
    const item = this.getItemInCartByProductId(productId);
    return (item ? item.quantity : 0);
  }

  getTotalPdtQtyInCart(): number {
    let sum = 0;
    const cart = this.shoppingCartBehaviorSubject.value;
    if (cart) {
      cart.items.forEach(item => {
        sum += item.quantity;
      });
    }
    return sum;
  }

  getCartTotalPrice(): number {
    let total = 0;
    const cart = this.shoppingCartBehaviorSubject.value;
    if (cart) {
      cart.items.forEach(item => {
        total += (item.quantity * item.product.price);
      });
    }
    return total;
  }

  async createCart(): Promise<string> {
    const defaultCart = this.getDefaultCartObject();
    const docRef = await this.shoppingCartsCollection.add(defaultCart);
    this.shoppingCartId = docRef.id;
    await this.getOrCreateCartDoc();
    return docRef.id;
  }

  async clearCart() {
    const cartDoc = await this.getOrCreateCartDoc();
    localStorage.removeItem('shopping-cart-id');
    return await cartDoc.delete();
  }

  private getDefaultCartObject() {
    const defaultCart: ShoppingCart = {
      userId: this.userId,
      createdDate: new Date(),
      lastUpdateDate: new Date(),
      items: []
    };

    return defaultCart;
  }

  private async getOrCreateCartId() {
    if (!this.shoppingCartId) {
      // create cart
      const newCartId = await this.createCart();
      localStorage.setItem('shopping-cart-id', newCartId);
      this.shoppingCartId = newCartId;
    }
    return this.shoppingCartId;
  }

  private async getOrCreateCartDoc() {
    if (!this.shoppingCartDocument) {
      const cartId = await this.getOrCreateCartId();
      this.shoppingCartDocument = this.afs.doc<ShoppingCart>(`shopping-carts/${cartId}`);
      this.shoppingCartDocument.valueChanges().subscribe(cart => {
        this.shoppingCartBehaviorSubject.next(cart);
      });
    }
    return this.shoppingCartDocument;
  }

}
