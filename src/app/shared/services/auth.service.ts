import { UserService } from './user.service';
import { User } from '../models/user';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<User>;
  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private userService: UserService) {
    this.user$ = this.afAuth.authState
      .pipe(
        switchMap(firebaseUser => {
          if (!firebaseUser) {
            return of(null);
          }
          return this.afs.doc<User>(`users/${firebaseUser.uid}`).valueChanges();

        })
      );
  }

  loginWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.afAuth.auth.signInWithPopup(provider)
      .then(credentials => {
        this.userService.updateUserAfterLogin(credentials.user);
      });
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  isAllowedAsAmin(user: User) {
    if (!user || !user.roles) {
      return false;
    }
    return user.roles.includes('admin');
  }
}
