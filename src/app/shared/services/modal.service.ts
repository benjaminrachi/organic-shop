import { ModalContent } from '../models/modal-content';
import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../../core/components/modal/modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private modalService: NgbModal) { }

  open(modalContent: ModalContent) {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.modalContent = modalContent;
  }

}
