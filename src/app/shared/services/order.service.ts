import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Order } from '../models/order';
import { ShoppingCartService } from './shopping-cart.service';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  userId: string;
  ordersCollection: AngularFirestoreCollection<Order>;

  constructor(
    private afs: AngularFirestore,
    private auth: AuthService,
    private shoppingService: ShoppingCartService) {

    this.auth.user$.subscribe(user => {
      this.userId = user && user.uid;
    });
    this.ordersCollection = this.afs.collection<Order>('orders');
   }

  async createOrder(order: Order): Promise<string> {
    // creating order
    const newOrder = JSON.parse(JSON.stringify(order));
    const docRef = await this.ordersCollection.add(newOrder);
    // removing shopping cart --storage also
    await this.shoppingService.clearCart();
    return docRef.id;
  }

  updateOrder(newOrder: Order, orderId: string) {
    return this.afs.doc<Order>(`orders/${orderId}`).set(newOrder, { merge: true });
  }

  getOrderDoc(orderId): Observable<Order> {
    return this.afs.doc<Order>(`orders/${orderId}`).valueChanges();
  }

  getAllOrders(): Observable<Order[]> {
    return this.ordersCollection.valueChanges({ idField: 'id' });
  }

  getOrdersByUserId(): Observable<Order[]> {
    return this.afs.collection<Order>('orders', orders =>
      orders.where('userId', '==', (this.userId || '')).where('userId', '>', ''))
      .valueChanges();
  }
}
