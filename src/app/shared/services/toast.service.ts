import { Injectable, TemplateRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  toasts: any[] = [];
  errorTemplate = `Oops ! An error occured `;
  constructor() { }

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  showSuccess(text) {
    this.show((text || 'Success!'), { classname: 'bg-success text-light', delay: 3 * 1000 });
  }

  showError(text = this.errorTemplate) {
    this.show(text, { classname: 'bg-danger text-light', delay: 3 * 1000 });
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
}
