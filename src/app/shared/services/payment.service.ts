import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Payment } from '../models/payment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  paymentsCollection: AngularFirestoreCollection<Payment>;

  constructor(private afs: AngularFirestore) {
    this.paymentsCollection = this.afs.collection<Payment>('payments');
  }

  async createPayment(payment: Payment): Promise<string> {
    const docRef = await this.paymentsCollection.add(payment);
    return docRef.id;
  }

}
