import { Injectable } from '@angular/core';
import { Order } from '../models/order';

declare var StripeCheckout: StripeCheckoutStatic;

@Injectable({
  providedIn: 'root'
})
export class PaymentStripeService {
  handler: StripeCheckoutHandler;

  constructor() { }

  initHandler(callBackFn) {
    this.handler = StripeCheckout.configure({
      key: 'pk_test_ukwly2LS9hq7t2BM4cgAUnxi00fquyq65n',
      locale: 'auto',
      source: (sourceResponse) => callBackFn(sourceResponse)
    });
  }

  // To test things out, use card number 4242 4242 4242 4242
  // with any expiration date in the future, any 3 - digit number for the CVC and any valid zip code.
  checkout(order: Order, orderId: string) {
    // this.initHandler(callBackFn);
    this.handler.open({
      name: 'Oshop store',
      description: `Payment for order #${orderId}`,
      amount: order.totalPrice * 100,
    });
  }
}
