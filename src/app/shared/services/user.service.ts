import { User } from '../models/user';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private afs: AngularFirestore) { }

  updateUserAfterLogin(firebaseUser: firebase.User) {
    const newData: User = {
      displayName: firebaseUser.displayName,
      email: firebaseUser.email,
      phoneNumber: firebaseUser.phoneNumber,
      photoURL: firebaseUser.photoURL,
      providerId: firebaseUser.providerId,
      uid: firebaseUser.uid,
    };

    const userDoc = this.afs.doc<User>(`users/${firebaseUser.uid}`);
    userDoc.valueChanges().pipe(take(1))
      .subscribe(userFromDB => {
        if (userFromDB && !userFromDB.roles) {
          newData.roles = ['customer'];
        }
        // return userDoc.set(newData, { merge: true });
      });
  }
}
