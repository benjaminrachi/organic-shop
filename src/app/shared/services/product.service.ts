import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  productCollection: AngularFirestoreCollection<Product>;

  constructor(private afs: AngularFirestore) {
    this.productCollection = this.getProductCollection();
  }

  create(product: Product) {
    return this.productCollection.add(product);
  }

  getProductCollection() {
    return this.afs.collection<Product>('products');
  }

  getAllProducts() {
    return this.productCollection.valueChanges({ idField: 'id' });
  }

  getProduct(productId): Observable<Product> {
    return this.afs.doc<Product>(`products/${productId}`).valueChanges();
  }

  update(productId, newProduct) {
    return this.afs.doc<Product>(`products/${productId}`).set(newProduct, { merge: true });
  }

  delete(productId) {
    return this.afs.doc<Product>(`products/${productId}`).delete();
  }
}
