import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// components
import { ProductCardComponent } from './components/product-card/product-card.component';
import { UpdateProductQtyComponent } from './components/update-product-qty/update-product-qty.component';
import { OrdersTableComponent } from './components/orders-table/orders-table.component';

// services
import { AuthService } from './services/auth.service';
import { CategoryService } from './services/category.service';
import { ModalService } from './services/modal.service';
import { OrderService } from './services/order.service';
import { PaymentStripeService } from './services/payment-stripe.service';
import { PaymentService } from './services/payment.service';
import { ProductService } from './services/product.service';
import { ShoppingCartService } from './services/shopping-cart.service';
import { ToastService } from './services/toast.service';
import { UserService } from './services/user.service';

// guards
import { AuthGuard } from './guards/auth-guard.guard';

// pipes
import { ItemQtyGtZeroPipe } from './pipes/item-qty-gt-zero.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    ProductCardComponent,
    UpdateProductQtyComponent,
    OrdersTableComponent,
    ItemQtyGtZeroPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    FontAwesomeModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    NgbModule,
    FontAwesomeModule,
    ProductCardComponent,
    UpdateProductQtyComponent,
    OrdersTableComponent,
    ItemQtyGtZeroPipe,
  ],
  providers: [
    AuthService,
    CategoryService,
    ModalService,
    OrderService,
    PaymentStripeService,
    PaymentService,
    ProductService,
    ShoppingCartService,
    ToastService,
    UserService,
    AuthGuard,

  ]
})
export class SharedModule { }
