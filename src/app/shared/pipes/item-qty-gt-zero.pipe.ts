import { ShoppingCartItem } from '../models/shopping-cart';
import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
  name: 'itemQtyGtZero',
  pure: false
})
@Injectable()
export class ItemQtyGtZeroPipe implements PipeTransform {

  transform(items: ShoppingCartItem[] ) {
    return items.filter(item => item.quantity > 0);
  }

}
