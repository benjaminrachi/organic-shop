export interface Product {
  title: string;
  categoryId: string;
  price: number;
  imageUrl: string;
  id?: string;
}
