import { ShoppingCart } from './shopping-cart';
import { Product } from './product';

export class Order {
  id?: string;
  userId: string;
  createdDate: Date;
  items: OrderItem[];
  totalPrice: number;
  shippingDetails: ShippingDetails;
  status: string;

  constructor(userId: string, shippingDetails: ShippingDetails, cart: ShoppingCart) {
    this.userId = userId;
    this.createdDate = new Date();
    this.shippingDetails = shippingDetails;
    this.status = 'NOT PAID';
    this.items = cart.items.map(item => {
      return {
        product: item.product,
        quantity: item.quantity,
        itemTotalPrice: (item.product.price * item.quantity)
      } as OrderItem;
    });
    this.totalPrice = this.items.reduce(((previousTotal, newItem) => previousTotal + newItem.itemTotalPrice ) , 0);
  }
}

export interface OrderItem {
  product: Product;
  quantity: number;
  itemTotalPrice: number;
}

export interface ShippingDetails {
  name: string;
  addressline1: string;
  addressline2: string;
  city: string;
}
