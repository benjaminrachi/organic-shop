export interface Payment {
  id?: string;
  apiId: string;
  apiName: string;
  userId: string;
  createdDate: Date;
  orderId: string;
  amount: number;
  status: string;
  creditCardDetails: CreditCardDetails;
}

export interface CreditCardDetails {
  exp_month: string;
  exp_year: string;
  last4: string;
  country: string;
  brand: string;
}
