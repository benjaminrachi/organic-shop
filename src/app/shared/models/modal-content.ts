export interface ModalContent {
  title: string;
  body: string;
  actionButtons: ActionButton[];
}

export interface ActionButton {
  name: string;
  classes: string;
  click: () => {};
}
