import { Product } from './product';

export interface ShoppingCart {
  id?: string;
  userId?: string;
  createdDate: Date;
  lastUpdateDate: Date;
  items: ShoppingCartItem[];
}

export interface ShoppingCartItem {
  product: Product;
  quantity: number;
}
