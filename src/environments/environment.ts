// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCn6IN8VGlM_B8mhUTMXfOXPtpHnBFrJuc",
    authDomain: "organic-shop-32039.firebaseapp.com",
    databaseURL: "https://organic-shop-32039.firebaseio.com",
    projectId: "organic-shop-32039",
    storageBucket: "organic-shop-32039.appspot.com",
    messagingSenderId: "622303861823",
    appId: "1:622303861823:web:e1cac4141b169c64df2c8a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
